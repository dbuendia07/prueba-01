Diego Buendia

#  CLASE 01 "BACK END" - LECCIONES APRENDIDAS

![](FEVSBE.jpg)


## Un desarrollador Front-end:

Encargado de la parte interactiva de la aplicación y el usuario.

## Tecnologías de desarrollo que un Front-End debe conocer por niveles:

###    *Básico: HTML, CSS, Java Script, Bootstrap
###    *Intermedio: Angular, npm, reactjs, Vuejs, Webpack, Material UI.
###    *Pro: Docker, CircleCI, Kubernetes.

## Actividades de un Front-end

#### Ineracción a nivel UX
#### Conversión diseños web a HTML
#### Diseños Responsive
#### Integración con servicios para interacción dinámica de contenidos.

## Empresas que aportan al desarrollo Front-end

A continuación las empresas que aportan al FRON END, usando el framework REACT JS :
#### AIRBNB
#### NETFLIX
####  WHATSAPP
#### INSTAGRAM
#### SLACK
#### UBER

## ¿Cuál es el rol de un Front-end en las empresas del Perú?

Diseñar y desarrollar una interfaz de acceso que pueda cumplir con las necesidades y comportamiento de los usuarios quienes hacen uso de la aplicación incluyendo la experiencia del usuario y la integración con el backgraound quien brinda la información requerida por los usuarios.

## Tendencias para Front-end 2020:

#### PWA, Ejemplos de wmpresas que hace uso en sus aplicaciones: ALIBABA, TWITTER, NIKKEI,

#### INFRAESTRUCTURA EN LA NUBE, importante para el trabajo colaborativo con infraestructura. Ejm: Bit, Heroku, AWS, AZURE.


# Un desarrollador Back-end:

Encargado del desarrollo del BACKOFFICE, que es quien atiende todas las peticiones del usuario que interactúa desde el frontend, brindando toda la integración con los servicios,  el procesamiento de la información almacenada en las bases de datos demandada con las seguridad que amerita en cada solicitud realizada por los usuarios.

## Tecnologías de desarrollo que un Back-End debe conocer

##### Lenguajes de Programación y Frameworks
##### Bases de Datos
##### Administración de Servidores Web
##### Infraestructura Cloud
##### Protocolos SSL, HTTP, HTTPS, SMTP, FTP, SFTP, otros.

## Lenguajes de programación y Frameworks

You can save any file of the workspace to **Google Drive**, **Dropbox** or **GitHub** by opening the **Synchronize** sub-menu and clicking **Save on**. Even if a file in the workspace is already synced, you can save it to another location. StackEdit can sync one file with multiple locations and accounts.

##### Python y sus framework (Django y Flask)
##### PHP y sus Framework (Sinfony, Laravel)
##### Ruby
##### Node.js
##### ASP.NET

## Bases de datos

##### MySQL
#### SQL
#### Postgres
#### MongoDB
#### Oracle
#### SqlLite
#### RavenDB
#### Redis
#### Arango


## Servidores web y protocolos

### Nginx (Servidor HTTP)
### Gunicorn (Servidor HTTP)
### UWSGI (Servidor HTTP)
### Uvicorn (Servidor HTTP)
### Apache (Servidor HTTP)

### HTTP y HTTPS (Protocolo de TRansferecia de Datos con su evolución de seguridad la https).



# Actividades de un Back-end

### Analizar y diseñar la lógica del negocio.
### Diseñar y modelar la estructura de la BD.
### Integrar con el FRONT END los servicios con la cordinación del Dev. del Front End.
### Modelar y prototipar la aplicación y gestionar las versiones de los cambios.
### Asegurar la seguridad del flujo de la información y la infraestructura quien la soporta.
### Asegurar el correcto despliegue y cuplir con la lógica del negocio.



## Conceptos que debe conocer un desarrollador Back-end

### Performance:
Respuesta ótima anta la concurrencia de varios usuarios.

### Infraestructura:
Las herramientas físicas y digitales para poder cumplir con el despliegue del proyecto, entre estos son Hostings, servidores con características según el proyecto, programas como los lenguajes a utilizar y las herramientas colaborativas digitales existentes.

### Automatización:
Procesos que se realizan automáticamente, estos son programado dea cuerdo a la lógica del negocio o una taréa específica.


## Tendencias para Back-end 2020:

Programas más populares:

- Python
- Ruby
- Scala